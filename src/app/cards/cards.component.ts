import { Component } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent {
  cards: any;
constructor(private service:CustomerService, private route:Router){

}
ngOnInit(){

}
addToLikes(cards:any){
  this.service.addToLikes(cards);
}
goToPage() {
  this.route.navigate(['/hotels']);

}
// searchText: string='';

//  onSearchTextEntered(searchValue:string){
//   this.searchText =searchValue;
//   console.log(this.searchText);
//  }

}
