import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { HomepageComponent } from './homepage/homepage.component';
import { AuthGuard } from './auth.guard';
import { LikesComponent } from './likes/likes.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ContactComponent } from './contact/contact.component';
import { CardsComponent } from './cards/cards.component';
import { AboutComponent } from './about/about.component';
import { ReviewComponent } from './review/review.component';
import { ProfileComponent } from './profile/profile.component';
import { PaymentComponent } from './payment/payment.component';
import { HydComponent } from './hyd/hyd.component';
const routes: Routes = [
  { path: "login", component: LoginComponent },
  { path: "registration", component: RegistrationComponent },
 
 
//     {path: "homepage", canActivate:[AuthGuard], component:HomepageComponent},
//    {path: "likes", canActivate:[AuthGuard], component:LikesComponent},
  {
    path: "Navbar",
    component: NavbarComponent,
    children: [
      {path:"contact",  component: ContactComponent },
      {path:"about",  component: AboutComponent },
      {path:"review",  component: ReviewComponent },
      { path: "hyd", component: HydComponent },
      { path: "Profile", component: ProfileComponent },

      { path: "", redirectTo: "home", pathMatch: "full" },
      { path: "home", component: HomepageComponent },
      { path: "homepage", component: HomepageComponent },
      { path: "likes", component: LikesComponent },
     {path:"payment",  component:PaymentComponent}
      // { path: "registration", component: RegistrationComponent },
     
    ],
  },
   { path: "", redirectTo: "login", pathMatch: "full" },
{ path: "**", redirectTo: "/navbar", pathMatch: "full" },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
