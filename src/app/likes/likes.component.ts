import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-likes',
  templateUrl: './likes.component.html',
  styleUrls: ['./likes.component.css']
})
export class LikesComponent {
cards:any;
likesItems:any;
constructor(private service:CustomerService){
  this.likesItems = [];
  // this.cards=this.service.likesItems;
}
ngOnInit(){
  this.service.getCardsStatus().subscribe((cardsData: any)=>{
    this.likesItems.push(cardsData);
  });
}
}
