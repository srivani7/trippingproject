import { Component } from '@angular/core';
import { RouterLink } from '@angular/router';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
likesItems:any;
constructor(private service:CustomerService){
  this.likesItems = this.service.likesItems;
}
}
