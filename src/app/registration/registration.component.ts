
// import { Component } from '@angular/core';
// import { FormBuilder, FormGroup, Validators } from '@angular/forms';  // import Validators
// import { Router } from '@angular/router';
// import { CustomerService } from '../customer.service';
// import { async } from '@angular/core/testing';

// @Component({
//   selector: 'app-registration',
//   templateUrl: './registration.component.html',
//   styleUrls: ['./registration.component.css']
// })
// export class RegistrationComponent {

//   user:any;
//   registerForm!: FormGroup;
//   formData: any;

//   constructor(private fb: FormBuilder, private service: CustomerService, private router: Router) {  // inject FormBuilder service

//     this.user = { user_id: '', username: '', email: '', password: '', };
//   }

//   ngOnInit() {   // initialize form on component load
//     this.registerForm = this.fb.group({
//       customerName: ['', Validators.required],
//       phonenumber: ['', Validators.required],
//       email: ['', [Validators.required, Validators.email]],
//       password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(20), Validators.pattern('(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+ :;\'?/.,-])(?!.*\\s).*$')]],
//       confirmPassword: ['', Validators.required],
//     }, { 
//       validators: this.passwordMatchValidator 
//     });    
//   }

//   registerCust(formData: any) {
//     console.log(formData);
//     this.user.username = formData.customerName;
//     this.user.email = formData.email;
//     this.user.password = formData.password;
//     console.log(this.user);

//     this.service.register(this.user).subscribe((data:any) => {
//       console.log(data);
//     });
//   }
// }



import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent {

  user: any;
  registerForm!: FormGroup;
  formData: any;
  CustData:any;

  constructor(private fb: FormBuilder, private service: CustomerService, private router: Router) {
    this.user = { user_id: '', username: '', phonenumber:'', email: '', password: '' };
  }

  ngOnInit() {
    this.registerForm = this.fb.group({
      customerName: ['', Validators.required],
      phonenumber: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(20), Validators.pattern('(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+ :;\'?/.,-])(?!.*\\s).*$')]],
    });
  }

  registerCust(formData: any) {
    console.log(formData);
    this.user.username = formData.customerName;
    this.user.phonenumber = formData.phonenumber;
    this.user.email = formData.email;
    this.user.password = formData.password;
    console.log(this.user);
    this.service.register(this.user).subscribe((data:any) => {
      console.log(data);
      this.CustData=data;
    });

   
      // this.router.navigate(['/login']);
    

  }

}



