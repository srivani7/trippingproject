import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  likesItems:any;
  cardsToBeAdded:Subject<any>;
  // order: any;
  // httpOptions = {
  //   headers: new HttpHeaders({
  //     'Content-Type': 'application/json'
  //   })
  // };
  
  constructor(private http: HttpClient) {
    this.likesItems = [];
    this.cardsToBeAdded = new Subject();
  }
   addToLikes(cards:any){
    this.likesItems.push(cards);
    this.cardsToBeAdded.next(cards);
   }
getCardsStatus():any{
  return this.cardsToBeAdded.asObservable();
}

  register(user: any) {
    return this.http.post('http://localhost:8085/registerCustomer/', user) ;
  }

  getUser(email:any,password:any):any{
    return this.http.get('http://localhost:8085/login/'+email+'/'+password) ;
  }
  // createOrder(order: { name: any; email: any; phone: any; amount: any }): Observable<any> {
  //   console.log('amount is : ');
  //   console.log(order.amount);
  //   return this.http.post("http://localhost:8085/customers/createOrder", {
  //     userName: order.name,
  //     emailID: order.email,
  //     phoneNumber: order.phone,
  //     amount: order.amount
  //   }, this.httpOptions);
  // }

}
